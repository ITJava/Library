ALTER TABLE authors
ADD FOREIGN KEY (firstname) REFERENCES
 firstnames(id),
ADD FOREIGN KEY (lastname) REFERENCES
 lastnames(id),
ADD FOREIGN KEY (middlename) REFERENCES
 middlenames(id),
MODIFY alias VARCHAR(200);