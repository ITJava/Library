ALTER TABLE authors
CHANGE firstname
id_firstname BIGINT UNSIGNED NOT NULL,
CHANGE lastname
id_lastname BIGINT UNSIGNED NOT NULL,
 CHANGE middlename
id_middlename BIGINT UNSIGNED NOT NULL;