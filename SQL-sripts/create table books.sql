create table books (id serial,
title varchar(100) not null,
id_author bigint UNSIGNED not null,
description text(500),
isbn varchar(100),
cover_path text(500), 
release_date date,
primary key (id),
FOREIGN key (id_author) REFERENCES author(id))
CHARSET utf8;