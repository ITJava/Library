create table author (id serial,
firstname varchar(50) not null,
lastname varchar(100),
middlename varchar(255),
alias text(300),
birth_date date,
death_date date,
PRIMARY KEY (id))
CHARSET UTF8;