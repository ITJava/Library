package test;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import sample.MainTest;
import sample.log.LibraryLogger;

public class TestRunner {

    public static void main(String[] args) {
        Result result = JUnitCore.runClasses(MainTest.class);
        for(Failure failure : result.getFailures()){
            LibraryLogger.error(failure.getMessage());
        }
    }
}
