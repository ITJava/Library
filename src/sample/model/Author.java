package sample.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.time.LocalDate;

public class Author {
    private StringProperty firstName = new SimpleStringProperty("");
    private StringProperty middleName = new SimpleStringProperty("");
    private StringProperty lastName = new SimpleStringProperty("");
    private StringProperty alias = new SimpleStringProperty("");
    private LocalDate year = LocalDate.now();
    private LocalDate deathDay;
    private int id;
    private static int count = 0;

    public Author(String firstName) {
        setFirstName(firstName);
    }

    public Author(String firstName, LocalDate year) {
        setFirstName(firstName);
        this.year = year;
    }

    public String getFirstName() {
        return firstName.get();
    }

    public StringProperty firstNameProperty() {
        return firstName;
    }

    private void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    public String getAlias() {
        return alias.get();
    }

    public StringProperty aliasProperty() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias.set(alias);
    }

    public LocalDate getYear() {
        return year;
    }

    public void setYear(LocalDate year) {
        this.year = year;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getDeathDay() {
        return deathDay;
    }

    public void setDeathDay(LocalDate deathDay) {
        this.deathDay = deathDay;
    }

    public String getMiddleName() {
        return middleName.get();
    }

    public StringProperty middleNameProperty() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName.set(middleName);
    }

    public String getLastName() {
        return lastName.get();
    }

    public StringProperty lastNameProperty() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Author author = (Author) o;

        return id == author.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public String toString() {
        return firstName.get() + " " + middleName.get() + " " + lastName.get();
    }

    public boolean isDuplicate(Author author) {
        return this.getFirstName().equals(author.getFirstName()) &&
                this.getMiddleName().equals(author.getMiddleName()) &&
                this.getLastName().equals(author.getLastName()) &&
                this.getAlias().equals(author.getAlias()) &&
                this.getYear().equals(author.getYear());
    }
}
