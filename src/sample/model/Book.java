package sample.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.time.LocalDate;

public class Book {
    private static int idCounter = 0;
    private long id;
    private StringProperty title = new SimpleStringProperty();
    private Author author;
    private StringProperty authorName = new SimpleStringProperty();
    private LocalDate year = LocalDate.now();
    private StringProperty description = new SimpleStringProperty();
    private StringProperty isbn = new SimpleStringProperty();
    private StringProperty coverImagePath = new SimpleStringProperty();

    public Book(String title, Author author) {
        setTitle(title);
        setAuthor(author);
    }

    public static int getIdCounter() {
        return idCounter++;
    }

    public StringProperty isbnProperty() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn.set(isbn);
    }

    public String getIsbn() {
        return isbn.get();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title.get();
    }

    public StringProperty titleProperty() {
        return title;
    }

    public void setTitle(String title) {
        this.title.set(title);
    }

    public Author getAuthor() {
        return author;
    }

    public Author authorProperty() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
        authorName.set(author.getFirstName());
    }

    public String getAuthorName() {
        return authorName.get();
    }

    public StringProperty authorNameProperty() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName.set(authorName);
    }

    public LocalDate getYear() {
        return year;
    }

    public void setYear(LocalDate year) {
        this.year = year;
    }

    public String getDescription() {
        return description.get();
    }

    public StringProperty descriptionProperty() {
        return description;
    }

    public void setDescription(String description) {
        this.description.set(description);
    }

    public String getCoverImagePath() {
        return coverImagePath.get();
    }

    public StringProperty coverImagePathProperty() {
        return coverImagePath;
    }

    public void setCoverImagePath(String coverImagePath) {
        this.coverImagePath.set(coverImagePath);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        return id == book.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
