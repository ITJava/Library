package sample;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import sample.database.DBConnector;
import sample.log.LibraryLogger;
import sample.model.Author;
import sample.model.Book;
import sample.myinterface.AddAuthorListener;

import java.io.File;


public class MainScreenController {
    @FXML
    private TableView table;
    @FXML
    private TableColumn titleColumn;
    @FXML
    private TableColumn authorColumn;
    @FXML
    private TableColumn yearColumn;
    @FXML
    private ComboBox authorInput;
    @FXML
    private TextField isbnInput;
    @FXML
    private TextArea descriptionInput;
    @FXML
    private TextField coverImageInput;
    @FXML
    private DatePicker yearInput;
    @FXML
    private TextField titleInput;

    private ObservableList<Book> books;
    private ObservableList<Author> authors;
    private FileChooser chooser = new FileChooser();
    private File file;
    private AddAuthorListener authorListener;

    public MainScreenController() {
        System.out.println("Controller constructor");
    }

    void setBooks(ObservableList<Book> books) {
        this.books = books;
        bindTable();
    }

    public void setAuthors(ObservableList<Author> authors) {
        this.authors = authors;
        bindAuthors();
    }

    void setAuthorListener(AddAuthorListener authorListener) {
        this.authorListener = authorListener;
    }

    @FXML
    public void initialize() {
        System.out.println("In innitController");
        bindTable();
        bindAuthors();
    }

    private void bindTable() {
        if (books != null) {
            System.out.println("Bind Table. Book size: " + books.size());
            titleColumn.setCellValueFactory(new PropertyValueFactory<Book, String>("title"));
            yearColumn.setCellValueFactory(new PropertyValueFactory<Book, String>("year"));
            authorColumn.setCellValueFactory(new PropertyValueFactory<Book, String>("authorName"));
            table.setItems(books);
        }
    }

    private void bindAuthors(){
        if(authors != null){
            authorInput.setItems(authors);
        }
    }

    public void newButton(ActionEvent actionEvent) {
        clearFields();
    }

    private void clearFields(){
        titleInput.setText("");
        authorInput.getSelectionModel().clearSelection();
        yearInput.getEditor().clear();
        isbnInput.setText("");
        descriptionInput.setText("");
        coverImageInput.setText("");
    }

    public void chooseButton(ActionEvent actionEvent) {
        File file = chooser.showOpenDialog(null);
        if(file != null && file.exists() && !file.isDirectory()){
            coverImageInput.setText(file.getAbsolutePath());
        } else if (this.file == null){
            coverImageInput.setText("");
        }
        if(file!=null){
            this.file=file;
        }
    }

    public void onSaveBookClicked(ActionEvent actionEvent) {
        Author author = (Author) authorInput.getValue();
        if(author == null){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Author field is empty");
            alert.show();
        } else {
            Book book = new Book(titleInput.getText(), author);
            book.setCoverImagePath(file.getAbsolutePath());
            book.setDescription(descriptionInput.getText());
            book.setIsbn(isbnInput.getText());
            book.setYear(yearInput.getValue());
            books.add(book);
            clearFields();
        }
    }

    public void showDialog(ActionEvent actionEvent) {
        authorListener.onAddAuthorClicker();
    }

    public void importDB(ActionEvent actionEvent) {
        if(DBConnector.getInstance().importDB()){
            LibraryLogger.log("Import finished normal");
        } else {
            LibraryLogger.log("Import does not finished");
        }
    }

    public void exportDB(ActionEvent actionEvent) {
        if(DBConnector.getInstance().exportDB()){
            LibraryLogger.log("Export finished normal");
        } else {
            LibraryLogger.log("Export does not finished");
        }
    }
}
