package sample.myinterface;

public interface ImportExportDatabase {
    boolean importDatabase();
    boolean exportDatabase();
}
