package sample.log;

import org.pmw.tinylog.Configurator;
import org.pmw.tinylog.Level;
import org.pmw.tinylog.Logger;
import org.pmw.tinylog.writers.ConsoleWriter;
import org.pmw.tinylog.writers.FileWriter;

public class LibraryLogger {
    static {
        Configurator.defaultConfig()
                .addWriter(new FileWriter("log.txt"))
                .addWriter(new ConsoleWriter())
                .level(Level.INFO)
                .activate();
    }
    public static void log(String log){
        Logger.info(log);
    }

    public static void error(String log){
        Logger.error(log);
    }
}
