package sample;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import sample.database.DBConnector;
import sample.database.DataLayer;
import sample.model.Author;
import sample.model.Book;
import sample.myinterface.AddAuthorListener;

import java.util.Random;

public class Main extends Application implements AddAuthorListener {

    private ObservableList<Book> bookObservableList = FXCollections.observableArrayList();
    private ObservableList<Author> authorObservableList = FXCollections.observableArrayList();
    private final DataLayer dataLayer;

    public Main() {

        dataLayer = new DataLayer();
        System.out.println("In Constructor");
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        System.out.println("Start");
//        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("layout/sample.fxml"));
        GridPane root = loader.load();
        MainScreenController controller = loader.getController();
        System.out.println("Controller retrieved");
        controller.setBooks(bookObservableList);
        controller.setAuthors(authorObservableList);
        controller.setAuthorListener(this);
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 400, 375));
        primaryStage.show();
        authorObservableList.addAll(dataLayer.getAuthorList());
        bookObservableList.addAll(dataLayer.getBookList());

        System.out.println("");

    }

    @Override
    public void init() throws Exception {
        super.init();
        System.out.println("Init");
    }

    public static void main(String[] args) {
        DBConnector.getInstance(); //just init
        launch(args);
        DBConnector.getInstance().close();
    }

    @Override
    public void onAddAuthorClicker() {
        new AuthorCreateDialog(this);
    }

    void addAuthorInDB(Author author) {
        for (Author a : authorObservableList) {
            if (a.isDuplicate(author)) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText("This author is duplicate");
                alert.show();
                return;
            }
        }
        Author newAuthor = dataLayer.addAuthorInDB(author);
        if (newAuthor != null) {
            authorObservableList.add(newAuthor);
        }
    }

    public Author getSomeAuthor(){
        Random random = new Random();
        return authorObservableList.get(random.nextInt(authorObservableList.size()));
    }
}
