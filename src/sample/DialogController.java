package sample;

import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

public class DialogController {
    @FXML
    private TextField firstName;
    @FXML
    private DatePicker deathDay;
    @FXML
    private TextField middleName;
    @FXML
    private TextField lastName;
    @FXML
    private TextField alias;
    @FXML
    private DatePicker year;


    public TextField getAlias() {
        return alias;
    }

    public DatePicker getYear() {
        return year;
    }

    public TextField getFirstName() {
        return firstName;
    }

    public DatePicker getDeathDay() {
        return deathDay;
    }

    public TextField getMiddleName() {
        return middleName;
    }

    public TextField getLastName() {
        return lastName;
    }
}
