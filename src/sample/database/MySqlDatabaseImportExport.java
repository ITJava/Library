package sample.database;

import sample.log.LibraryLogger;
import sample.myinterface.ImportExportDatabase;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MySqlDatabaseImportExport implements ImportExportDatabase {
    private DBConnector dbConnector = DBConnector.getInstance();

    @Override
    public boolean importDatabase() {
        String sql = "SHOW VARIABLES LIKE 'secure_file_priv';";
        ResultSet rs = dbConnector.executeQuery(sql);
        try {
            rs.next();
            String path = rs.getString("Value");
            LibraryLogger.log("Path to import database from file" + path);
            File lastnames = new File(path + "lastnames.csv");
            File middlenames = new File(path + "middlenames.csv");
            File firstnames = new File(path + "firstnames.csv");
            File authors = new File(path + "authors.csv");
            File books = new File(path + "books.csv");
            if (lastnames.exists() && middlenames.exists() && firstnames.exists() &&
                    authors.exists() && books.exists()) {
                if (dbConnector.executeStatement("LOAD DATA INFILE '" + lastnames.getAbsolutePath().replaceAll("\\\\", "/") +
                        "' INTO TABLE lastnames;") &&
                        dbConnector.executeStatement("LOAD DATA INFILE '" + middlenames.getAbsolutePath().replaceAll("\\\\", "/") +
                                "' INTO TABLE middlenames;") &&
                        dbConnector.executeStatement("LOAD DATA INFILE '" + firstnames.getAbsolutePath().replaceAll("\\\\", "/") +
                                "' INTO TABLE firstnames;") &&
                        dbConnector.executeStatement("LOAD DATA INFILE '" + authors.getAbsolutePath().replaceAll("\\\\", "/") +
                                "' INTO TABLE authors;") &&
                        dbConnector.executeStatement("LOAD DATA INFILE '" + books.getAbsolutePath().replaceAll("\\\\", "/") +
                                "' INTO TABLE books;")) {
                    return true;
                }
            }
        } catch (SQLException e) {
            LibraryLogger.error(e.getMessage());
        }
        return false;
    }

    @Override
    public boolean exportDatabase() {
        String sql = "SHOW VARIABLES LIKE 'secure_file_priv';";
        ResultSet rs = dbConnector.executeQuery(sql);
        try {
            rs.next();
            String path = rs.getString("Value");
            File lastNamePath = new File(path + "lastnames4.csv");
            File middleNamePath = new File(path + "middlenames4.csv");
            File firstNamePath = new File(path + "firstnames4.csv");
            File authorsPath = new File(path + "authors4.csv");
            File booksPath = new File(path + "books4.csv");
            sql = "SELECT * INTO OUTFILE '" + firstNamePath.getAbsolutePath().replaceAll("\\\\", "/") + "' FROM firstnames;";
            dbConnector.executeStatement(sql);
            sql = "SELECT * INTO OUTFILE '" + lastNamePath.getAbsolutePath().replaceAll("\\\\", "/") + "' FROM lastnames;";
            dbConnector.executeStatement(sql);
            sql = "SELECT * INTO OUTFILE '" + middleNamePath.getAbsolutePath().replaceAll("\\\\", "/") + "' FROM middlenames;";
            dbConnector.executeStatement(sql);
            sql = "SELECT * INTO OUTFILE '" + authorsPath.getAbsolutePath().replaceAll("\\\\", "/") + "' FROM authors;";
            dbConnector.executeStatement(sql);
            sql = "SELECT * INTO OUTFILE '" + booksPath.getAbsolutePath().replaceAll("\\\\", "/") + "' FROM books;";
            dbConnector.executeStatement(sql);
            return true;
        } catch (SQLException e) {
            LibraryLogger.error(e.getMessage());
        }
        return false;
    }
}
