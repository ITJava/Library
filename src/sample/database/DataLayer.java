package sample.database;

import sample.log.LibraryLogger;
import sample.model.Author;
import sample.model.Book;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class DataLayer {
    private List<Author> authorList = new ArrayList<>();
    private List<Book> bookList = new ArrayList<>();

    public DataLayer() {
        initialize();
    }

    private void initialize() {
        authorList = DBConnector.getInstance().getAuthors();
        bookList = loadBooks();
    }

    private List<Book> loadBooks() {
        List<Book> books = new ArrayList<>();
        ResultSet rs = DBConnector.getInstance().executeQuery("select * from books;");
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                int authorId = rs.getInt("id_author");
                String tittle = rs.getString("title");
                String description = rs.getString("description");
                String isbn = rs.getString("isbn");
                String coverPath = rs.getString("cover_path");
                Date releaseDate = rs.getDate("release_date");

                Book book = new Book(tittle, getAuthorById(authorId));
                book.setId(id);
                book.setDescription(description);
                book.setIsbn(isbn);
                book.setCoverImagePath(coverPath);
                book.setYear(releaseDate == null ? null : releaseDate.toLocalDate());

                books.add(book);
            }
            rs.close();
        } catch (SQLException e) {
            LibraryLogger.error(e.getMessage());
        }
        return books;
    }

    private Author getAuthorById(int id) {
        for (Author author : authorList) {
            if (author.getId() == id)
                return author;
        }
        return null;
    }

    public Author addAuthorInDB(Author author) {
        String sql;
        String birthDate = author.getYear() == null ? null : "'" + author.getYear() + "'";
        String deathDate = author.getDeathDay() == null ? null : "'" + author.getDeathDay() + "'";
        String alias = author.getAlias() == null || author.getAlias().isEmpty() ?
                null : "'" + author.getAlias() + "'";
        sql = "INSERT INTO authors (id_firstname, id_middlename, id_lastname, alias, birth_date, death_date) " +
                "VALUES (" + selectOrInsertFirstName(author.getFirstName()) + ", " +
                selectOrInsertMiddleName(author.getMiddleName()) + ", " +
                selectOrInsertLastName(author.getLastName()) + ", " +
                alias + ", " + birthDate + ", " + deathDate + ");";
        DBConnector.getInstance().executeStatement(sql);
        return DBConnector.getInstance().getAuthorFromDB(author);
    }

    private long selectOrInsertFirstName(String firstName) {
        String sql;
        sql = "SELECT id FROM firstnames WHERE fname like '" + firstName + "';";
        ResultSet rs = DBConnector.getInstance().executeQuery(sql);
        try {
            if (rs.first()) {
                long id;
                id = rs.getLong("id");
                return id;
            } else {
                DBConnector.getInstance().executeStatement(
                        "INSERT INTO firstnames (fname) VALUES ('" + firstName + "');");
                rs = DBConnector.getInstance().executeQuery(sql);
                rs.next();
            }
            return rs.getLong("id");
        } catch (SQLException e) {
            LibraryLogger.error(e.getMessage());
        }
        return -1;
    }

    private long selectOrInsertMiddleName(String middleName) {
        String sql;
        sql = "SELECT id FROM middlenames WHERE mname like '" + middleName + "';";
        ResultSet rs = DBConnector.getInstance().executeQuery(sql);
        try {
            if (rs.next()) {
                return rs.getLong("id");
            } else {
                DBConnector.getInstance().executeStatement(
                        "INSERT INTO middlenames (mname) VALUES ('" + middleName + "');");
                rs = DBConnector.getInstance().executeQuery(sql);
                rs.next();
            }
            return rs.getLong("id");
        } catch (SQLException e) {
            LibraryLogger.error(e.getMessage());
        }
        return -1;
    }

    private long selectOrInsertLastName(String lastName) {
        String sql;
        sql = "SELECT id FROM lastnames WHERE lname like '" + lastName + "';";
        ResultSet rs = DBConnector.getInstance().executeQuery(sql);
        try {
            if (rs.next()) {
                return rs.getLong("id");
            } else {
                DBConnector.getInstance().executeStatement(
                        "INSERT INTO lastnames (lname) VALUES ('" + lastName + "');");
                rs = DBConnector.getInstance().executeQuery(sql);
                rs.next();
            }
            return rs.getLong("id");
        } catch (SQLException e) {
            LibraryLogger.error(e.getMessage());
        }
        return -1;
    }



    public List<Author> getAuthorList() {
        return authorList;
    }

    public List<Book> getBookList() {
        return bookList;
    }
}
