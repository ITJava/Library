package sample.database;

import sample.log.LibraryLogger;
import sample.model.Author;
import sample.model.Book;
import sample.myinterface.ImportExportDatabase;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBConnector {
    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_URL = "jdbc:mysql://localhost:3306";
    private static final String USER = "root";
    private static final String PASS = "admin";
    private static DBConnector dbConnector;
    private Connection conn;
    private ImportExportDatabase importExportDatabase;
    private PreparedStatement pStat;


    private DBConnector() {
        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            conn.setAutoCommit(true);
            conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            Statement stmt = conn.createStatement();
            String sql;
            sql = "USE library";
            stmt.execute(sql);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            //TODO create database
        }
    }

    private void init() {
        importExportDatabase = new MySqlDatabaseImportExport();
    }

    boolean executeStatement(String sql) {
        try {
            conn.createStatement().execute(sql);
        } catch (SQLException e) {
            LibraryLogger.error(e.getMessage());
            return false;
        }
        return true;
    }

    ResultSet executeQuery(String sql) {
        try {
            pStat = conn.prepareStatement(sql);
            return pStat.executeQuery();
        } catch (SQLException e) {
            LibraryLogger.error(e.getMessage());
            return null;
        }
    }

    public boolean importDB() {
        return importExportDatabase.importDatabase();
    }

    public boolean exportDB() {
        return importExportDatabase.exportDatabase();
    }

    public void close() {
        try {
            conn.close();
        } catch (SQLException e) {
            LibraryLogger.log(e.getMessage());
        }
    }

    public static DBConnector getInstance() {
        try {
            Class.forName(DBConnector.class.getName());
        } catch (ClassNotFoundException e) {
            //Could never happen
            System.exit(0);
        }
        synchronized (DBConnector.class) {
            if (dbConnector == null) {
                dbConnector = new DBConnector();
                dbConnector.init();
            }
            return dbConnector;
        }
    }

    List<Author> getAuthors() {
        List<Author> authors = new ArrayList<>();
        try {
            String sql = "SELECT " +
                    "a.id, " +
                    "f.fname firstname, " +
                    "m.mname middlename, " +
                    "l.lname lastname, " +
                    "a.alias, " +
                    "a.birth_date, " +
                    "a.death_date " +
                    "FROM authors a  " +
                    "join firstnames f on a.id_firstname = f.id " +
                    "join middlenames m on a.id_middlename = m.id " +
                    "join lastnames l on a.id_lastname = l.id;";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int authorId = rs.getInt("id");
                String firstName = rs.getString("firstname");
                String middleName = rs.getString("middlename");
                String lastName = rs.getString("lastname");
                String authorAlias = rs.getString("alias");
                Date date = rs.getDate("birth_date");
                Date dateGone = rs.getDate("death_date");

                Author author = new Author(firstName);
                author.setId(authorId);
                author.setMiddleName(middleName);
                author.setLastName(lastName);
                author.setAlias(authorAlias);
                author.setYear(date == null ? null : date.toLocalDate());
                author.setDeathDay(dateGone == null ? null : dateGone.toLocalDate());

                authors.add(author);
            }
            rs.close();
            preparedStatement.close();
            return authors;
        } catch (SQLException e) {
            LibraryLogger.error(e.getMessage());
        }
        return null;
    }

    Author getAuthorFromDB(Author author){
        String sql = "SELECT  " +
                "a.id, " +
                "f.fname firstname, " +
                "m.mname middlename, " +
                "l.lname lastname, " +
                "a.alias, " +
                "a.birth_date, " +
                "a.death_date " +
                "FROM authors a  " +
                "join firstnames f on a.id_firstname = f.id " +
                "join middlenames m on a.id_middlename = m.id " +
                "join lastnames l on a.id_lastname = l.id " +
                "WHERE fname like '" + author.getFirstName() + "' AND " +
                "mname like '" + author.getMiddleName() + "' AND " +
                "lname like '" + author.getLastName() + "' AND " +
                "birth_date like '" + author.getYear() + "';";
        ResultSet rs = executeQuery(sql);
        try {
            if(rs.first()){
                int authorId = rs.getInt("id");
                String firstName = rs.getString("firstname");
                String middleName = rs.getString("middlename");
                String lastName = rs.getString("lastname");
                String authorAlias = rs.getString("alias");
                Date date = rs.getDate("birth_date");
                Date dateGone = rs.getDate("death_date");
                Author newAuthor = new Author(firstName);
                newAuthor.setId(authorId);
                newAuthor.setMiddleName(middleName);
                newAuthor.setLastName(lastName);
                newAuthor.setAlias(authorAlias);
                newAuthor.setYear(date == null ? null : date.toLocalDate());
                newAuthor.setDeathDay(dateGone == null ? null : dateGone.toLocalDate());
                return newAuthor;
            }
        } catch (SQLException e) {
            LibraryLogger.error(e.getMessage());
        }
        return null;
    }
}
