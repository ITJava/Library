package sample;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.layout.AnchorPane;
import sample.model.Author;

import java.io.IOException;
import java.util.Optional;

class AuthorCreateDialog extends Dialog<Author> {

    public AuthorCreateDialog(Main main) {
        Main main1 = main;

        setTitle("Create Author");
        setHeaderText("Input author data");
        ButtonType loginButtonType = new ButtonType("Create", ButtonBar.ButtonData.OK_DONE);
        getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

// Enable/Disable login button depending on whether a username was entered.
//        Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
//        loginButton.setDisable(true);

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("layout/dialog.fxml"));
        AnchorPane root = null;
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        DialogController controller = loader.getController();
        getDialogPane().setContent(root);

// Request focus on the username field by default.
        //Platform.runLater(() -> username.requestFocus());

        setResultConverter(dialogButton -> {
            if (dialogButton == loginButtonType) {
                Author author = new Author(controller.getFirstName().getText(),
                        controller.getYear().getValue());
                author.setMiddleName(controller.getMiddleName().getText());
                author.setLastName(controller.getLastName().getText());
                author.setAlias(controller.getAlias().getText());
                author.setDeathDay(controller.getDeathDay().getValue());
                if(isValid(author)) {
                    return author;
                }
            }
            return null;
        });

        Optional<Author> result = showAndWait();
        result.ifPresent(main::addAuthorInDB);
    }

    private boolean isValid(Author author){
        if(author.getFirstName().isEmpty() || author.getYear() == null){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("All fields obligatory");
            alert.show();
            return false;
        }
        return true;
    }
}
